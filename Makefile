.PHONY: init docs-publish docs-dev launch-cluster delete-cluster
.SILENT:

init:
	python3 -m pip install --upgrade pip setuptools poetry
	poetry install --with docs

docs-dev:
	poetry run mkdocs serve

docs-publish:
	@if [ $$(git branch --show-current) == main ]; then ALIAS=stable; else ALIAS=$$(git branch --show-current); fi; poetry run mike deploy -b pages --update-aliases --push $$(poetry version -s) $${ALIAS}
