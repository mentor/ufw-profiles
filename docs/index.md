# UFW Profiles

<!-- begin badges -->

[![pre-commit enabled][pre-commit badge]][pre-commit project]
[![Black codestyle][black badge]][black project]

[pre-commit badge]: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
[pre-commit project]: https://pre-commit.com/
[black badge]: https://img.shields.io/badge/code%20style-black-000000.svg
[black project]: https://github.com/psf/black

<!-- end badges -->

A collection of application profiles for the Uncomplicated FireWall (UFW).

## 📚 Documenation

Documentation is hosted with GitHub Pages and can be found at:

- ✅ [ufw-profiles/stable](https://mentor.codeberg.page/ufw-profiles/stable) (reflecting the `Master` branch); or
- 🔥 [ufw-profiles/develop](https://mentor.codeberg.page/ufw-profiles/develop) (for bleeding edge).

## 🚀 Usage

Assuming you have `ufw` enabled and installed the profiles contained within this repository can be dropped into `/etc/ufw/applications.d/` and simply enabled with `sudo ufw allow {profile name}`.

Some things worth considering:

- Limiting access certain IP ranges.
- Limiting access to certain interfaces.

**IP Range Limiting (CIDR notation)**

```zsh
sudo ufw allow from 192.168.0.0/24 to any app avahi
sudo ufw allow to 192.168.0.0/24 from any app avahi
```

**Limiting access to a specific interface**

```zsh
sudo ufw allow in on enp0s2 to any app avahi
sudo ufw allow out on enps0 from any app avahi
```
